# Dar - lightweight markup language

Dar™ ([Hebrew word](https://he.wiktionary.org/wiki/%D7%93%D7%A8#%D7%93%D6%B7%D6%BC%D7%A8) for [nacre, mother of pearl](https://en.wikipedia.org/wiki/Nacre)) is a [lightweight markup language](https://en.wikipedia.org/wiki/Lightweight_markup_language) developed for the project [Pninim](https://pninim.org) (Hebrew word for pearls). It was not possible to rely on Markdown since its link syntax - `[...](...)` is used in some editions of Talmud for other purposes...

There were more reasons for creating yet another markup language though:

## Goals

### Evolution

Syntax should be able to evolve. If needs be, even in a backwards incompatible manner. At the same time old documents should always remain renderable and editable without conversion.

To this end [(semantic) versioning](https://semver.org/) has been introduced. Particular syntax version can be activated, used and then deactivated (even multiple times in the middle of the same file).


### Unambiguity

For the sake of cleanliness and particularly in a case of collaborative editing - there should be no alternative syntax option (like `-`, `+` or `*` for a bullet list marker). Utilizing a character for only one purpose: if `*` is used for emphasis - do not use it also as a bullet list marker.


### Consistent links syntax...

Full form:

- `[Pninim Project]<https://pninim.org Pninim Project Website>`
- `[My poems]</home/user/my_poems.txt>`

Short form: 

- `<https://pninim.org Pninim Project Website>`
- `</home/user/my_poems.txt>`


### ...and the corresponding file inclusion

Full form (with optional caption):

- `+[Me 10 years ago]</home/user/me.png My passport photo>`
- `+[My poems]<my_poems.txt>`

Short form:

- `+</home/user/me.png>`
- `+<my_poems.txt>`


### Even more lightweight

Easily readable structured short links like `<My Book:Volume I:Chapter 8:Primary Topic>` enabled by [URI aliases](uri_aliases.dar) and [URI detector](uri_detector.dar).


### Laconic

Keeping markup to an absolute minimum: why \*\*bold\*\* if it can be just \*bold\*?


### Clarity

Blocks with opening and closing syntax:

\`{  
Code block  
\`}

instead of

\```  
Code block  
\```

Useful for the understanding where a block starts and where it ends especially when there are several consecutive blocks.


### Intuitive

\*bold\*, /italic/, \_underlined\_, ~strike-through~.


### Non-Latin, right-to-left languages support

No alphabetic elements in syntax (contrary to ORG).


## Syntax

For the informal syntax specification consult [syntax.dar](syntax.dar).


## Enable dar in Gedit

Restart [Gedit](https://wiki.gnome.org/Apps/Gedit) after copying [dar.lang](https://gitlab.com/pninim.org/dar/dar-syntax/-/raw/master/dar.lang) file to the following location:

### Linux

`/usr/share/gtksourceview-3.0/language-specs` or `~/.local/share/gtksourceview-3/language-specs`

### Windows

`C:\Program Files\gedit\share\gtksourceview-3.0\language-specs`


## Copyright, trademark, etc..

[MIT License](LICENSE)

Copyright 2019-2021 Zeev Pekar <https://dar.pninim.org>

Trademark: Dar™  
Logo mark: (.)™ - resembles a pearl between shells.

