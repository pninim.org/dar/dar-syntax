% URI detector

Dar provides the freedom to structure complex documents using directories/files (on the file system level) and headers (on the document level). This allows to split longer section into separate files at any time. URI detector facilitates easy creation of readable short form links within that documents structure.

For example, let's assume following hierarchy:

My_Book/
├── Volume_I
│   ├── Part_1
│   ├── Part_2
│   ├── Part_3
│   │   └── chapter_8.dar
│   └── Part_4
├── Volume_II
└── Volume_III

While chapter_8.dar contains these headers:

% Major Topic
...
%% Third Subtopic

First we supply the location of My_Book through declaration of an [URI alias]<uri_aliases.dar>:
# <My Book>: ~/Documents/My_Book

Now, due to URI detector, we can use short form links such as this <My Book:volume I:part 3:chapter 8:Major Topic:Third Subtopic>. Note, that the different document's structure levels within the link are provided in easily readable form, i.e. whitespaces instead of `_`, no `.dar` file extension, etc.. They do not exactly reflect real directory and file names on the file system. That is the URI detector who is responsible for determining the precise location of the referenced file based on such a loose information provided in the link. Following scheme is being used in order to precisely map a documents structure level mentioned in URI to the corresponding directory, file or a header:

1. try exact search
2. try case insensitive search
3. try to replace each whitespace in URI with `_`
4. try to add `.dar` extension and test whether such a file exists
5. once file was detected - continue with headers search

If, for some reason, URI detector detects several possible candidates satisfying a particular URI - it should link to the first one it founds, but warn about the existence of others.
